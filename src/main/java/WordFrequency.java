public class WordFrequency {
    private String word;
    private int wordCount;

    public WordFrequency(String word, int wordCount) {
        this.word = word;
        this.wordCount = wordCount;
    }


    public String getWord() {
        return this.word;
    }

    public int getWordCount() {
        return this.wordCount;
    }


}
