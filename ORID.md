**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview:我代码中存在许多问题，比如：测试中使用for减少代码量，使用stream出现逻辑错误，代码中还有很多无用变量，测试应该将不同的测试类抽离出来。
2. Refactor：学习了一些Code Small类别：Mysterious Name，duplicated code，long function等，在我们进行refactor时首先应进行test protection，在测试样例全部测试通过后，开始寻找code smell，并对code smell进行refactor and make sure，在每次修改后测试用例全部通过后进行baby step。

**R (Reflective): Please use one word to express your feelings about today's class.**

反思

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

在今天的作业中，我深刻体会到一个不好的代码习惯会造成后续维护的不便，日后的编写代码时我们也需要提高代码质量。

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

refactor的代码快捷键，以及前端refactor的代码快捷键，在课后我需要多去学习这方面知识