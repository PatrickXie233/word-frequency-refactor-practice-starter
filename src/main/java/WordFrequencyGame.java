import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencyList = getWordFrequencyList(inputStr);
            Map<String, List<WordFrequency>> wordFrequencyListMap = getWordFrequencyListMap(wordFrequencyList);
            List<WordFrequency> wordFrequenciesGroupByWord = getWordFrequenciesGroupByWord(wordFrequencyListMap);
            return getStringJoiner(wordFrequenciesGroupByWord);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private static String getStringJoiner(List<WordFrequency> wordFrequenciesGroupByWord) {
        return wordFrequenciesGroupByWord.stream()
                .map(wordFrequency -> String.format("%s %s",wordFrequency.getWord(),wordFrequency.getWordCount()))
                .collect(Collectors.joining("\n"));
    }

    private static List<WordFrequency> getWordFrequenciesGroupByWord(Map<String, List<WordFrequency>> wordFrequencyListMap) {
        List<WordFrequency> wordFrequencyList = new ArrayList<>();
        wordFrequencyListMap.forEach((key, value) -> wordFrequencyList.add(new WordFrequency(key, value.size())));
        wordFrequencyList.sort((w1, w2) -> w2.getWordCount() - w1.getWordCount());
        return wordFrequencyList;
    }

    private static List<WordFrequency> getWordFrequencyList(String inputStr) {
        String[] inputStrList = inputStr.split("\\s+");
        return Arrays.stream(inputStrList)
                .map(str -> new WordFrequency(str, 1))
                .collect(Collectors.toList());
    }


    private Map<String, List<WordFrequency>> getWordFrequencyListMap(List<WordFrequency> wordFrequencyList) {
        Map<String, List<WordFrequency>> map = new HashMap<>();
        wordFrequencyList.forEach(input -> map.computeIfAbsent(input.getWord(), k -> new ArrayList<>()).add(input));
        return map;
    }


}
